//
//  FileReader.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/16/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation

class FileReader {
    
    
    static func readJSONDateFromFile(fileName: String) -> Data?
    {
        if let path = Bundle.main.path(forResource: fileName, ofType: "") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                // Getting data from JSON file using the file URL
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                return data
            } catch {
                // Handle error here
            }
        }
        return nil
    }
    
}



