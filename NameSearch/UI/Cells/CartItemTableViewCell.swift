import UIKit

protocol CartItemTableViewCellDelegate {
    func didRemoveFromCart(selectedRow: Int)
}

class CartItemTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var removeButton: UIButton!
    
    var selecetedRow: Int = 0

    @IBAction func removeFromCartButtonTapped(_ sender: UIButton) {

        delegate?.didRemoveFromCart(selectedRow: selecetedRow)
    }

    var delegate: CartItemTableViewCellDelegate?
}
