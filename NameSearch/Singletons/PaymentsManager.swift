import Foundation

class PaymentsManager: NSObject{
    static var shared = PaymentsManager()

    //var selectedPaymentMethod: PaymentMethod?
    var userPayments: [PaymentMethod]?
    var userPaymentsLoadState: LoadState = .unloaded
    @objc dynamic var didUpdatePayments: Int = 0
}
