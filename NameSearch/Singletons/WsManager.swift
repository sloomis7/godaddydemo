//
//  WsManager.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/15/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation


protocol WsManagerProtocol {
    
    func login(params:[String:String], completion: @escaping (Data?, CustomError?) -> Void)
    func domainSearch(queryItems: String, completion: @escaping (Data?, CustomError?) -> Void)
    func suggestedDomainSearch(queryItems: String, completion: @escaping (Data?, CustomError?) -> Void)
    func processPayment(params: [String:String], completion: @escaping (Data?, CustomError?) -> Void)
    func getUserPaymentMethods(completion: @escaping (Data?, CustomError?) -> Void)
    var testCase: WSManagerTestCase { get set }
    
    
}

class WsManager: WsManagerProtocol {
    
    static let shared = WsManager()
    
    var testCase: WSManagerTestCase = .none
    
    
    private let requestManager: RequestManagerProtocol = RequestManager.shared
    
    func login(params: [String:String], completion: @escaping (Data?, CustomError?) -> Void) {
        
        let url = URL(string: GetUrl.getUrl(.login))!
        
        requestManager.post(params, url){ data, error in
            
            if let data = data, error == nil {
                
                DispatchQueue.main.async {
                    completion(data, nil)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
            
        }
        
    }
    
    
    func domainSearch(queryItems: String, completion: @escaping (Data?, CustomError?) -> Void) {
        
        var urlComponents = URLComponents(string: GetUrl.getUrl(.domainSearch))!
        urlComponents.queryItems = [
            URLQueryItem(name: "q", value: queryItems)
        ]
        requestManager.get(urlComponents.url!) { data, error in
            
            if let data = data, error == nil {
                
                DispatchQueue.main.async {
                    completion(data, nil)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
            
        }
        
    }
    
    func suggestedDomainSearch(queryItems: String, completion: @escaping (Data?, CustomError?) -> Void) {
        
        var urlComponents = URLComponents(string: GetUrl.getUrl(.suggestedDomainSearch))!
        urlComponents.queryItems = [
            URLQueryItem(name: "q", value: queryItems)
        ]
        
        requestManager.get(urlComponents.url!) { data, error in
            
            if let data = data, error == nil {
                
                DispatchQueue.main.async {
                    completion(data, nil)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
            
        }
        
    }
    
    func processPayment(params: [String:String], completion: @escaping (Data?, CustomError?) -> Void) {
        
        let url = URL(string: GetUrl.getUrl(.payment))!
        
        requestManager.post(params, url){ data, error in
            
            if let data = data, error == nil {
                
                DispatchQueue.main.async {
                    completion(data, nil)
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
            }
            
        }
        
    }
    
    func getUserPaymentMethods(completion: @escaping (Data?, CustomError?) -> Void) {
        
        let url = URL(string: GetUrl.getUrl(.userPayments))!
        
        
        self.requestManager.get(url) { data, error in
            //Simulate slow ws response
            // for some reason there sleep doenst allow another request call - have to do another thread to sleep
            DispatchQueue.global(qos: .userInitiated).async {
                //sleep(20)
                if let data = data, error == nil {
                    
                    DispatchQueue.main.async {
                        completion(data, nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(nil, error)
                    }
                }
            }
            
            
        }
        //}
        
        
        
        
        
    }
    
}

enum WSManagerTestCase {
    case none
    case success
    case error
}


class WsManagerMock: WsManagerProtocol {
    
    static let shared = WsManagerMock()
    
    var testCase: WSManagerTestCase = .none
    
    
    func login(params: [String : String], completion: @escaping (Data?, CustomError?) -> Void) {
        
        switch testCase {
        case .success:
            
            let data = FileReader.readJSONDateFromFile(fileName: "login")
            completion(data, nil)
            
        case .error:
            completion(nil, .clientError)
        default:
            completion(nil, nil)
        }
    }
    
    func domainSearch(queryItems: String, completion: @escaping (Data?, CustomError?) -> Void) {
        
    }
    
    func suggestedDomainSearch(queryItems: String, completion: @escaping (Data?, CustomError?) -> Void) {
        
    }
    
    func processPayment(params: [String : String], completion: @escaping (Data?, CustomError?) -> Void) {
        
    }
    
    func getUserPaymentMethods(completion: @escaping (Data?, CustomError?) -> Void) {
        
    }
}
