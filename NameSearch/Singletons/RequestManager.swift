//
//  RequestManager.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/15/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation

protocol RequestManagerProtocol {
    
    func get(_ url:URL, completion: @escaping (Data?, CustomError?) -> Void)
    func post(_ params:[String:String], _ url:URL, completion: @escaping (Data?, CustomError?) -> Void)

}

class RequestManager: RequestManagerProtocol {
    
    
    static let shared = RequestManager()
    
    // MARK: GET
    func get(_ url:URL, completion: @escaping (Data?, CustomError?) -> Void){
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if error != nil || data == nil {
                completion(nil, .clientError)
                return
            }
            
            if let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode){} else {
                completion(nil, .serverError)
                return
            }
            
            if let mime = response?.mimeType, mime == "application/json" {} else {
                completion(nil, .mimeTypeError)
                return
            }
            
            completion(data!, nil)
            
        })
        
        task.resume()
    }
    
    // MARK: POST
    func post(_ params:[String:String], _ url: URL, completion: @escaping (Data?, CustomError?) -> Void){
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: .fragmentsAllowed)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if error != nil || data == nil {
                completion(nil, .clientError)
                return
            }
            
            if let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode){} else {
                completion(nil, .serverError)
                return
            }
            
            if let mime = response?.mimeType, mime == "application/json" {} else {
                completion(nil, .mimeTypeError)
                return
            }
            
            completion(data!, nil)
        })
        
        task.resume()
    }
    
}
