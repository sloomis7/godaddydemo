import Foundation
import UIKit



class DomainSearchViewController: UIViewController {

    @IBOutlet var searchTermsTextField: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var cartButton: UIButton!
    
    private let viewModel: DomainSearchViewModel = DomainSearchViewModel(wsManager: WsManager())


    @IBAction func searchButtonTapped(_ sender: UIButton) {
        
        if searchTermsTextField.text != "" {
            
            searchTermsTextField.resignFirstResponder()
            viewModel.loadData(searchTerms: searchTermsTextField.text!) { [weak self] error in
                
                if let _ = self?.viewModel.data, error == nil {
                    
                    self?.tableView.reloadData()
                }
            }
        } else {
            let controller = UIAlertController(title: "Error", message: "Please Enter Search Terms", preferredStyle: .alert)

            let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
              })
            controller.addAction(ok)
            self.present(controller, animated: true)
        }
        
        
    }

    @IBAction func cartButtonTapped(_ sender: UIButton) {
        
        performSegue(withIdentifier: "showCheckout", sender: nil)

    }


    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated: false)
        configureCartButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.data = nil
        searchTermsTextField.text = ""
        configureCartButton()
        tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    

    private func configureCartButton() {
        
        if let data = viewModel.data {
            var found = false
            for domain in data {
                if domain.isSelected {
                    found = true
                    break
                }
            }
            
            if cartButton.isEnabled != found {
                cartButton.isEnabled = found
                cartButton.backgroundColor = cartButton.isEnabled ? .black : .lightGray
            }
        } else {
            cartButton.isEnabled = false
            cartButton.backgroundColor = cartButton.isEnabled ? .black : .lightGray
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! CartViewController
        
        var selectedDomains: [Domain] = []
        for domain in viewModel.data! {
            if domain.isSelected {
                selectedDomains.append(domain)
            }
        }
        vc.viewModel.selectedDomains = selectedDomains
    }
}

extension DomainSearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        cell.selectionStyle = .none
        let domain = viewModel.data![indexPath.row]

        cell.textLabel!.text = domain.name
        cell.detailTextLabel!.text = domain.price
        
        cell.backgroundColor = domain.isSelected ? .lightGray : .white

        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data?.count ?? 0
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension DomainSearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var domain = viewModel.data![indexPath.row]
        domain.isSelected = !domain.isSelected
        viewModel.data![indexPath.row] = domain
        
        tableView.reloadRows(at: [indexPath], with: .none)

        configureCartButton()
    }

}
