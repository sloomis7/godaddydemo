import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!

    private let viewModel: LoginViewModel = LoginViewModel(wsManager: WsManager())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if viewModel.bypassLogin {
            performSegue(withIdentifier: "showDomainSearch", sender: self)
        }
    }

    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        
        guard let dict = validateTextFields() else {
            // Alert
            alert()
            return
        }
        
        

        
        viewModel.doLogin(dict: dict) { [weak self] data, error in
            
            if let _ = data, error == nil {

                self?.performSegue(withIdentifier: "showDomainSearch", sender: self)
                
            }
            
        }
        
        
    }
    //Test for empty
    func validateTextFields() -> [String: String]? {
        
        guard usernameTextField.text != "", passwordTextField.text != "" else {
            return nil
        }
        
        
        let dict: [String: String] = [
            "username": usernameTextField.text!,
            "password": passwordTextField.text!
        ]
        return dict
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func alert(_ error: CustomError? = nil) {
        
        let controller = UIAlertController(title: "Oops!", message: error?.localizedDescription ?? "", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
          })
        controller.addAction(ok)

        self.present(controller, animated: true)
        
    }
}
