import UIKit



class PaymentMethodsViewController: UIViewController {

    @IBOutlet var tableView: UITableView!

    let viewModel: PaymentMethodsViewModel = PaymentMethodsViewModel(wsManager: WsManager())

    override func viewDidLoad() {
        super.viewDidLoad()
        
            viewModel.loadPaymentMethods { [weak self] state in
                
                switch state {
                case .error:
                    let controller = UIAlertController(title: "Oops!", message: "", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
                        self?.dismiss(animated: true, completion: nil)
                    })
                    controller.addAction(ok)

                    self?.present(controller, animated: true)
                default:
                    self?.tableView.reloadData()
                }
            }
            
        }
 
    
}

extension PaymentMethodsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PaymentsManager.shared.userPayments?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)

        let method = PaymentsManager.shared.userPayments![indexPath.row]

        cell.textLabel!.text = method.name

        if let lastFour = method.lastFour {
            cell.detailTextLabel!.text = "Ending in \(lastFour)"
        } else {
            cell.detailTextLabel!.text = method.displayFormattedEmail!
        }

        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let method = PaymentsManager.shared.userPayments![indexPath.row]
        dismiss(animated: true) {
            self.viewModel.delegate?.didSelectPaymentMethod(paymentMethod: method)
        }
    }
}
