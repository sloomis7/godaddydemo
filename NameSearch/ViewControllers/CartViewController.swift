import UIKit

class CartViewController: UIViewController {

    @IBOutlet var payButton: UIButton!
    @IBOutlet var tableView: UITableView!
    
    let viewModel: CartViewModel = CartViewModel(wsManager: WsManager())


    @IBAction func payButtonTapped(_ sender: UIButton) {
        if viewModel.selectedPaymentMethod == nil {
            self.performSegue(withIdentifier: "showPaymentMethods", sender: self)
        } else {
            payButton.isEnabled = false
            viewModel.performPayment { [weak self] error in
                var controller: UIAlertController!
                
                if let error = error {
                    
                    controller = UIAlertController(title: "Oops!", message: error.localizedDescription, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .cancel) { _ in
                        self?.payButton.isEnabled = true
                    }

                    controller.addAction(action)
                } else {
                    
                    controller = UIAlertController(title: "All done!", message: "Your purchase is complete!", preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "Ok", style: .default) { _ in
                        self?.navigationController?.popViewController(animated: true)

                    }

                    controller.addAction(action)
                }
                
                
                
                self?.present(controller, animated: true)

            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "CartItemTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "CartItemCell")
        updatePayButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    func updatePayButton() {
        if viewModel.selectedPaymentMethod == nil {
            payButton.setTitle("Select a Payment Method", for: .normal)
        } else {
            payButton.setTitle(viewModel.getPayButtonString(), for: .normal)
        }
    }

    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! PaymentMethodsViewController
        vc.viewModel.delegate = self
    }
}

extension CartViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.selectedDomains.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartItemCell", for: indexPath) as! CartItemTableViewCell

        cell.delegate = self
        cell.selecetedRow = indexPath.row
        
        cell.nameLabel.text = viewModel.selectedDomains[indexPath.row].name
        cell.priceLabel.text = viewModel.selectedDomains[indexPath.row].price

        return cell
    }
}

extension CartViewController: CartItemTableViewCellDelegate {
    func didRemoveFromCart(selectedRow: Int) {
        
        viewModel.selectedDomains.remove(at: selectedRow)
        
        if viewModel.selectedDomains.count == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            updatePayButton()
            tableView.reloadData()
        }
        
    }
}

extension CartViewController: PaymentMethodsViewModelDelegate {
    func didSelectPaymentMethod(paymentMethod: PaymentMethod) {
        viewModel.selectedPaymentMethod = paymentMethod
        updatePayButton()
    }
}
