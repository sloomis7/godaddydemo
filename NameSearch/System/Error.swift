//
//  Error.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/15/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation


enum CustomError: Error {
    // Throw when an invalid password is entered
    case invalidPassword
    
    
    case clientError
    case serverError
    case mimeTypeError
        
    case unexpected(code: Int)
}

enum LoadState {
    case unloaded
    case loading
    case loaded
    case error
}
