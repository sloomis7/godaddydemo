//
//  Urls.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/15/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation


enum UrlNames{
    case login
    case domainSearch
    case suggestedDomainSearch
    case payment
    case userPayments
}

class GetUrl{
    
    static func getUrl(_ url: UrlNames) -> String{
        
        switch enviroment {
        case .beta:
            return getUrlBeta(url)
        case .production:
            return getUrlProduction(url)
        }
        
    }
    
    static func getUrlBeta(_ url: UrlNames) -> String{
        let host = "http://127.0.0.1:5000/"
        return getFullUrl(host, url)
    }
    
    static func getUrlProduction(_ url: UrlNames) -> String{
        let host = "https://gd.proxied.io/"
        return getFullUrl(host, url)
    }
    
    static func getFullUrl(_ host: String, _ url: UrlNames) -> String{
        
        switch url {
        case .login:
            return host + "auth/login"
        case .domainSearch:
            return host + "search/exact"
        case .suggestedDomainSearch:
            return host + "search/spins"
        case .payment:
            return host + "payments/process"
        case .userPayments:
            return host + "user/payment-methods"
        }
        
        
    }
}
