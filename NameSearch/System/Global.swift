//
//  Global.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/15/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation


enum Enviroment {
    case beta, production
}


let enviroment: Enviroment = .production
