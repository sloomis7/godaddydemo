//
//  Domain.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/16/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation

struct Domain {
    let name: String
    let price: String
    let productId: Int
    
    var isSelected = false
}
