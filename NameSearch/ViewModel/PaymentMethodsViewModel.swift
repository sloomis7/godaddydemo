//
//  PaymentMethodsViewModel.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/16/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation

protocol PaymentMethodsViewModelDelegate {
    func didSelectPaymentMethod(paymentMethod: PaymentMethod)
}

class PaymentMethodsViewModel {
    
    var delegate: PaymentMethodsViewModelDelegate?

    private let wsManager: WsManagerProtocol!
    var kvoToken: NSKeyValueObservation?


    init(wsManager manager: WsManagerProtocol) {
        
        wsManager = manager
        start()
    }
    
    func start() {
        
    }
    
    deinit {
        kvoToken?.invalidate()
    }
    
    func loadPaymentMethods(completion: @escaping (LoadState) -> Void) {
        
        switch PaymentsManager.shared.userPaymentsLoadState {
        case .unloaded, .error:
            // Try reload
        
            PaymentsManager.shared.userPaymentsLoadState = .loading
            wsManager.getUserPaymentMethods { data, error in
                
                
                if let data = data, error == nil {
                    
                    let paymentMethods = try!
                        JSONDecoder().decode([PaymentMethod].self, from: data)
                    PaymentsManager.shared.userPayments = paymentMethods
                    PaymentsManager.shared.userPaymentsLoadState = .loaded

                } else {
                    PaymentsManager.shared.userPaymentsLoadState = .error
                }
                
                completion(PaymentsManager.shared.userPaymentsLoadState)
            }
        case .loading:
            
            kvoToken = PaymentsManager.shared.observe(\.didUpdatePayments, options: .new) { (person, change) in
                //guard let age = change.newValue else { return }
                //print("New age is: \(age)")
                
                completion(PaymentsManager.shared.userPaymentsLoadState)

            }
        default:
            // Do nothing
            completion(PaymentsManager.shared.userPaymentsLoadState)
        }
        
    }
    
    
}
