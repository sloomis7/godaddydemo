//
//  CartViewModel.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/16/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation

class CartViewModel {
    var selectedDomains: [Domain] = []
    var selectedPaymentMethod: PaymentMethod?
    private let wsManager: WsManagerProtocol!


    init(wsManager manager: WsManagerProtocol) {
        
        wsManager = manager
        start()
    }
    
    func start() {
        
    }
    
    func performPayment(completion: @escaping (CustomError?) -> Void) {

        let dict: [String: String] = [
            "auth": AuthManager.shared.token!,
            "token": selectedPaymentMethod!.token
        ]
        
        wsManager.processPayment(params: dict) { data, error in
            
            if let _ = data, error == nil {
                
                completion(nil)

            } else {
                
                completion(.clientError)
                
            }
        }
    }
    
    func getPayButtonString() -> String {
        
        var totalPayment = 0.00

        selectedDomains.forEach {
            let priceDouble = Double($0.price.replacingOccurrences(of: "$", with: ""))!
            totalPayment += priceDouble
        }

        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency

        return "Pay \(currencyFormatter.string(from: NSNumber(value: totalPayment))!) Now"
    }
    
    
}



