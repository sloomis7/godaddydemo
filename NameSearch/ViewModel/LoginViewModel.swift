//
//  LoginViewModel.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/15/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation

class LoginViewModel {
    
    private let wsManager: WsManagerProtocol!

    let bypassLogin: Bool
    
    
    init(wsManager manager: WsManagerProtocol, _ bypass: Bool = false) {
        
        wsManager = manager
        bypassLogin = bypass
        start()
    }
    
    func start() {
        
        if bypassLogin {
            
//            let user = User(first: "Scott", last: "Loomis")
//
//            AuthManager.shared.user = user
//            AuthManager.shared.token = "abc123"
            
            doLogin(dict: ["test": "test"]) { data, error in

            }


        }
    }
    
    func doLogin(dict: [String: String], completion: @escaping (Data?, CustomError?) -> Void) {
        
        let group = DispatchGroup()
        group.enter()
        
        wsManager.login(params: dict) { data, error in
            
            
            if let data = data, error == nil {
                
                let authReponse = try! JSONDecoder().decode(LoginResponse.self, from: data)

                AuthManager.shared.user = authReponse.user
                AuthManager.shared.token = authReponse.auth.token
                
            }
            
            completion(data, error)
            group.leave()
            
        }
        
        
        group.notify(queue: .main) { [weak self] in
         
            PaymentsManager.shared.userPaymentsLoadState = .loading
            self?.wsManager.getUserPaymentMethods { data, error in
                
                
                if let data = data, error == nil {
                    
                    let paymentMethods = try!
                        JSONDecoder().decode([PaymentMethod].self, from: data)
                    PaymentsManager.shared.userPayments = paymentMethods
                    PaymentsManager.shared.userPaymentsLoadState = .loaded

                } else {
                    PaymentsManager.shared.userPaymentsLoadState = .error
                }
                PaymentsManager.shared.didUpdatePayments += 1
            }
            
        }

    }

    
}
