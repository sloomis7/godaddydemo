//
//  DomainSearchViewModel.swift
//  NameSearch
//
//  Created by Scott Loomis on 5/16/21.
//  Copyright © 2021 GoDaddy Inc. All rights reserved.
//

import Foundation

class DomainSearchViewModel {
    
    var data: [Domain]?
    private let wsManager: WsManagerProtocol!


    init(wsManager manager: WsManagerProtocol) {
        
        wsManager = manager
        start()
    }
    
    func start() {
        
    }
    func loadData(searchTerms: String, completion: @escaping (CustomError?) -> Void) {
        
        let group = DispatchGroup()

        group.enter()
        group.enter()
        
        var exactMatchResponse: DomainSearchExactMatchResponse?
        var suggestionsResponse: DomainSearchRecommendedResponse?

        wsManager.domainSearch(queryItems: searchTerms) { data, error in
            
            if let data = data, error == nil {
                
                exactMatchResponse = try! JSONDecoder().decode(DomainSearchExactMatchResponse.self, from: data)
                
            }
            
            group.leave()
        }
        
        
        wsManager.suggestedDomainSearch(queryItems: searchTerms) { data, error in
            
            if let data = data, error == nil {
                suggestionsResponse = try! JSONDecoder().decode(DomainSearchRecommendedResponse.self, from: data)
            }
            
            group.leave()
        }
        
        group.notify(queue: .main) { [weak self] in
            
            self?.data = nil
            
            if let exactResponse = exactMatchResponse {
                
                let exactDomainPriceInfo = exactResponse.products.first(where: { $0.productId == exactResponse.domain.productId })!.priceInfo
                
                let exactDomain = Domain(name: exactResponse.domain.fqdn,
                                         price: exactDomainPriceInfo.currentPriceDisplay,
                                         productId: exactResponse.domain.productId)
                if self?.data == nil {
                    self?.data = [exactDomain]
                    
                } else {
                 
                    self?.data!.append(exactDomain)
                }
            }
            
            if let suggestResponse = suggestionsResponse {
                
                let suggestionDomains = suggestResponse.domains.map { domain -> Domain in
                    let priceInfo = suggestResponse.products.first(where: { price in
                        price.productId == domain.productId
                    })!.priceInfo

                    return Domain(name: domain.fqdn, price: priceInfo.currentPriceDisplay, productId: domain.productId)
                }
                
                if self?.data == nil {
                    self?.data = suggestionDomains
                    
                } else {
                 
                    for domain in suggestionDomains {
                        self?.data!.append(domain)
                    }
                }
                
            }
            
            completion(nil)
        }
    }
}

