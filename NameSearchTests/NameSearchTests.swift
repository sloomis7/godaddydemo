//
//  NameSearchTests.swift
//  NameSearchTests
//
//  Created by Mat Cartmill on 9/4/20.
//  Copyright © 2020 GoDaddy Inc. All rights reserved.
//

import XCTest
@testable import NameSearch

class NameSearchTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testLoginWS() throws {

        let wsManager = WsManagerMock()
        wsManager.testCase = .error
        let loginVM = LoginViewModel(wsManager: wsManager, false)
        let expectation = expectation(description: "SomeService does stuff and runs the callback closure")

        
        loginVM.doLogin(dict: ["test" : "test"]) { data, error in
            XCTAssertNil(AuthManager.shared.user)
            XCTAssertNil(AuthManager.shared.token)
        }
        
        
        wsManager.testCase = .success

        loginVM.doLogin(dict: ["test" : "test"]) { data, error in
            XCTAssertTrue(AuthManager.shared.user != nil)
            XCTAssertTrue(AuthManager.shared.token != nil)

            expectation.fulfill()

        }
        
        waitForExpectations(timeout: 10)

        //XCTAssertEqual(homeView.controller.test1(), "false")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
